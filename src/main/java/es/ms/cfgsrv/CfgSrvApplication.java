package es.ms.cfgsrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class CfgSrvApplication {

	public static void main(String[] args) {
		SpringApplication.run(CfgSrvApplication.class, args);
	}
}
